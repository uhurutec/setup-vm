#!/bin/bash
#
# Copyright (C) 2022-2024 UhuruTec AG <info@uhurutec.com>
# Copyright (C)      2024 Max Harmathy <harmathy@mailbox.org>
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify it in
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

###################
# Runtime context #
###################

shopt -s -o errexit
shopt -s -o errtrace
shopt -s -o functrace
shopt -s -o nounset
shopt -s -o pipefail
shopt -s dotglob
shopt -s globstar
shopt -s gnu_errfmt
shopt -s inherit_errexit
shopt -s lastpipe
shopt -s nullglob

#########################
# Globals and constants #
#########################

# runtime environment
this="$(basename "$0")"
min_curl_version=(7 73 0) # for curl parameter --output-dir

# VM os_name settings and defaults
default_os_name="debian-12"
default_disk_size="20G"
default_mem_size="4096" #4GiB
default_cpus="2"
default_image_format="qcow2"
default_connection_url="qemu:///system"
default_setup_timeout=300
fallback_connection_url="${LIBVIRT_DEFAULT_URI:-"${default_connection_url}"}"
default_pool='default'
default_network='default'
fallback_user_name="$(id -un)"
supported_images=(
  "debian-10"
  "debian-11"
  "debian-12"
  "ubuntu-18.04"
  "ubuntu-20.04"
  "ubuntu-22.04"
  "ubuntu-24.04"
  "fedora-39"
  "fedora-40"
  "fedora-41"
)

# log level
error_level="ERROR"
info_level="INFO"
debug_level="DEBUG"

#############################
# Logging and return values #
#############################

# This function reads a stream and prepends a prefix to each line and writes
# the lines back to stdout.
function prepend {
  local prefix="$1"
  while IFS='' read -r line; do
    printf "%s%s\n" "$prefix" "$line"
  done
}

# Takes an error message, writes it to the log file and prints it to stdout.
# This function also sets the status variable to SYSTEM_FAILURE_EXIT_CODE.
function error_line {
  echo -en "[$error_level] " >&2
  echo "$*" >&2
  global_status=1
}

# Takes all lines from stdin and logs it as error (see function error_line).
function error_log {
  local line
  while IFS='' read -r line; do
    error_line "$line"
  done
}

# Takes an info message, writes it to the log file and prints it to stdout.
function info_line {
  echo -en "[$info_level] "
  echo "$*"
}

# Takes all lines from stdin and logs it as info (see function info_line).
function info_log {
  local line
  while IFS='' read -r line; do
    info_line "$line"
  done
}

# Takes an debug message, writes it to the log file and prints it to stdout if
# variable debug_output is not empty.
function debug_line {
  if [ -n "$debug_output" ]; then
    echo -en "[$debug_level] "
    echo "$*"
  fi
}

# Takes all lines from stdin and logs it as debug message (see function
# debug_line).
function debug_log {
  local line
  while IFS='' read -r line; do
    debug_line "$line"
  done
}

# This function serves as a guard for the parameters of function run_and_log.
# It checks if the given parameter corresponds to one of the defined log levels.
function is_log_level {
  local level=$1
  case $level in
    debug | info | error)
      true
      ;;
    *)
      false
      ;;
  esac
}

# This function executes a command, while separately logging stdout and stderr.
# The first two parameters denote the log level for stdout and stderr
# respectively. All other parameters are considered to be a command, which will
# be executed. The log levels will be check using the is_log_level function to
# make sure they're correctly passed to the function.
function run_and_log {
  local stdout_level=$1
  local stderr_level=$2
  shift
  shift
  if ! is_log_level "$stdout_level" || ! is_log_level "$stderr_level"; then
    error_line "Tried to call $1 with unhandled log level"
    return "2"
  fi
  debug_line "Calling \"$*\""
  "$@" > >(prepend "> " | "${stdout_level}_log") 2> >(prepend "> " | "${stderr_level}_log" >&2)
}

# This is a convenience function, which calls run_and_log and sets stdout to
# debug and stderr to error. This covers the majority of calls to external
# programms, which assumes that the stdout contains only messages which are not
# relevant in this context and stderr contains only messages, which indicate a
# major failure preventing normal operation.
function run_and_log_debug {
  run_and_log "debug" "error" "$@"
}

# This function allows running a command transparently on the libvirt host,
# regardless whether the virtual machine is created locally or a remote libvirt
# machine via SSH.
# This function does not use logging and can therefore be used to process the
# pristine output from the command.
# The function run_and_log_command_on_libvirt_host on the other hand is suited
# to execute a command which performs a task on the worker.
function run_command_on_libvirt_host {
  local command=("$@")

  if [ -z "$remote_host" ]; then
    "${command[@]}"
  else
    # shellcheck disable=SC2029
    ssh "${remote_ssh_parameters[@]}" "$remote_host" "${command[@]}"
  fi
}

# This function executes the given command in the same way as the function
# run_command_on_libvirt_host.
# This function should be used for commands, which perform a task on the remote.
# On the other hand run_command_on_libvirt_host is used, when processing the
# pristine output of the command is required.
function run_and_log_command_on_libvirt_host {
  local command=("$@")

  if [ -n "$remote_host" ]; then
    debug_line "Executing on $remote_host"
  fi
  run_and_log_debug run_command_on_libvirt_host "${command[@]}"
}

#########################################
# Configuration, Environment and Checks #
#########################################

# Print all supported OS images
function print_supported_images {
  echo "Supported OS images are: ${supported_images[*]}"
}

# Takes an OS name and check if it is in the list of supported images.
function os_check_supported {
  local image=$1
  for supported in "${supported_images[@]}"; do
    if [ "$image" = "$supported" ]; then
      return
    fi
  done
  echo "OS \"$image\" is not (yet) supported. $(print_supported_images)" | fmt -w 72 | error_log
  return "1"
}

# This function takes an image name of one of the supported images and sets
# variables 'distribution', 'version', 'os_variant', 'template_image'
# and 'template_url' accordingly. Variable 'suite' is set for debian and
# derivates.
function distribution_set_details {
  local image_name=$1

  # split image name into parts
  IFS='-' read -r -a image_name_parts <<<"$image_name"

  distribution="${image_name_parts[0]}"
  version="${image_name_parts[1]}"

  case $distribution in
    debian | ubuntu)
      debian_derivatives_set_details
      ;;
    fedora)
      fedora_set_details
      ;;
  esac
}

# Select appropriate candidate from list of os variants;
# higher rank wrt. sort corresponds to higher precedence.
function select_os_variant {
  local -a candidates

  # compute intersection of available and given variants
  comm -12 \
    <(virt-install --os-variant list | sed '/^$/Q' | sed 's/, /\n/g' | sort) \
    <(IFS=$'\n'; echo "$*" | sort) \
    | readarray -t candidates
  if (( ${#candidates[@]} == 0 )); then
    echo "Unknown OS variant(s): $*" | fmt -w 72 | error_log
    return 1
  fi
  echo "${candidates[-1]}"
}

# Helper function for filtering the fedora image of the image list from the
# download server.
function path_from_fedora_list {
  local list=("$@")
  local pattern="./linux/(releases|development)/$version/Cloud/x86_64/images/Fedora-Cloud-Base-$version-.*.x86_64.qcow2"
  for image in "${list[@]}"; do
    if [[ "$image" =~ $pattern ]]; then
      echo "$image"
      return 0
    fi
  done

  # image wasn't found, error handling from here on
  base_pattern="\./linux/(releases|development)/$version/Cloud.*"
  local matching_base=()
    for image in "${list[@]}"; do
    if [[ "$image" =~ $base_pattern ]]; then
      matching_base+=("$image")
    fi
  done
  error_line "No fedora image found in list from server"
  if [ "${#matching_base[@]}" = '0' ]; then
    error_line "Base path for cloud images of fedora version $version is missing."
  else
    error_line "Base path for cloud images of fedora version $version is matching."
    error_line "However, the encoded pattern didn't match any image."
    error_line "Candidates are:"
    printf -- ' - %s\n' "${matching_base[@]}" | error_log
  fi
  return 1
}

# This function reads 'distribution' and 'version' variable and sets
# variables 'os_variant', 'template_image' and 'template_url' for fedora images.
function fedora_set_details {
  local url_prefix="https://download.fedoraproject.org/pub/fedora"
  local list_url="https://dl.fedoraproject.org/pub/fedora/imagelist-fedora"
  declare -a image_list
  readarray -t image_list < <(curl -s "$list_url")
  local image_path
  image_path="$(path_from_fedora_list "${image_list[@]}")"
  debug_line "$image_path"

  case $version in
    39)
      os_variant="$(select_os_variant fedora39)"
      ;;
    40)
      os_variant="$(select_os_variant fedora39 fedora40)"
      ;;
    41)
      os_variant="$(select_os_variant fedora39 fedora40 fedora41)"
      ;;
    *)
      echo >&2 "Unknown fedora version $version"
      return 1
      ;;
  esac

  template_image=${image_path##*/}
  template_url="$url_prefix${image_path:1}"
}

# This function reads 'distribution' and 'version' variable and sets
# variables 'os_variant', 'template_image' and 'template_url'
# for debian and derivative images.
function debian_derivatives_set_details {
  local suite
  case $distribution in
    debian)
      case $version in
        10)
          suite="buster"
          os_variant="$(select_os_variant debian10)"
          ;;
        11)
          suite="bullseye"
          os_variant="$(select_os_variant debian10 debian11)"
          ;;
        12)
          suite="bookworm"
          os_variant="$(select_os_variant debian10 debian11 debian12)"
          ;;
        *)
          echo >&2 "Unknown debian version $version"
          return 1
          ;;
      esac
      template_image="$distribution-$version-generic-amd64.qcow2"
      template_url="https://cloud.debian.org/images/cloud/$suite/latest/$template_image"
      ;;
    ubuntu)
      case $version in
        18.04)
          suite="bionic"
          os_variant="$(select_os_variant ubuntu18.04)"
          ;;
        20.04)
          suite="focal"
          os_variant="$(select_os_variant ubuntu18.04 ubuntu20.04)"
          ;;
        22.04)
          suite="jammy"
          os_variant="$(select_os_variant ubuntu18.04 ubuntu20.04 ubuntu22.04)"
          ;;
        24.04)
          suite="noble"
          os_variant="$(select_os_variant ubuntu18.04 ubuntu20.04 ubuntu22.04 ubuntu24.04)"
          ;;
        *)
          error_line "Unknown ubuntu version $version"
          return 1
          ;;
      esac
      template_image="$suite-server-cloudimg-amd64.img"
      template_url="https://cloud-images.ubuntu.com/$suite/current/$template_image"
      ;;
    *)
      error_line "Unknown distribution \"$distribution\""
      ;;
  esac
}

# This function returns whether the version of curl is greater or equal
# compared to the given version number "$major.$minor.$patch_level.
function check_curl_version {
  local major=$1
  local minor=$2
  local patch_level=$3

  local version_information
  read -ra version_information < <(run_command_on_libvirt_host curl --version)
  [ "${version_information[0]}" = "curl" ]

  local version_number
  IFS="." read -ra version_number <<<"${version_information[1]}"

  local index=0
  for component in "$major" "$minor" "$patch_level"; do
    if ((version_number[index] < component)); then
      return 1
    fi
    if ((version_number[index] > component)); then
      return 0
    fi
    ((index += 1))
  done
}

# This function checks connectivity to a remote machine via SSH.
function check_remote {
  # Before checking the actual SSH connection, check for potential problems in
  # name resolution which would otherwise be hard to detect.
  debug_line "Resolving libvirt remote machine:"
  local real_remote_host
  local -a ssh_connection_parameter
  # The remote host specified in libvirt URI isn't necessarily a host name but
  # could be an alias for a different host name or plain IP address and possibly
  # additional parameters (refer to manpage ssh_config(5) for details).
  while read -ra ssh_connection_parameter; do
    if [ "${ssh_connection_parameter[0]}" = "hostname" ]; then
      real_remote_host="${ssh_connection_parameter[1]}";
      break
    fi
  done < <(ssh -G "$remote_host")
  if [ -z "$real_remote_host" ]; then
    error_line "Couldn't determine real host from $remote_host"
  fi
  run_and_log_debug getent ahosts "$real_remote_host"

  debug_line "Test SSH connectivity to libvirt remote:"
  ssh "${remote_ssh_parameters[@]}" "$remote_host" true
  debug_line "SSH connectivity to libvirt remote established!"
}

# Retrieve path of libvirt pool given as parameter
function read_pool_path {
  local pool="$1"

  run_and_log_debug virsh pool-info --pool "$pool" >&2

  virsh pool-dumpxml "${pool}" | xmllint --xpath '//pool/target/path/text()' -
}

# This function checks the environment, if all necessary commands are available
# and if the image_directory is accessible.
function check_environment {
  local control_host_commands=(
    fmt
    getent
    ssh
    virsh
    virt-install
    xmllint
  )

  for cmd in "${control_host_commands[@]}"; do
    if ! type "$cmd" >/dev/null 2>&1; then
      error_line "Command $cmd not available!"
    fi
  done
  if [ "$global_status" != "0" ]; then
    return "$global_status"
  fi

  if [ -n "$remote_host" ]; then
    info_line "Connecting to remote $remote_host"
    check_remote
  fi

  local libvirt_host_commands=(
    cloud-localds
    curl
    genisoimage
    qemu-img
  )

  for cmd in "${libvirt_host_commands[@]}"; do
    if ! run_and_log_command_on_libvirt_host type "$cmd" >/dev/null 2>&1; then
      error_line "Command $cmd not available on libvirt host"
    fi
  done
  if [ "$global_status" != "0" ]; then
    return "$global_status"
  fi

  image_directory=$(read_pool_path "$pool")
  template_directory="$image_directory/templates"

  # check if image directory is accessible
  # Due to https://github.com/koalaman/shellcheck/issues/817
  # shellcheck disable=SC2154
  for dir in "$image_directory" "$template_directory"; do
    if ! run_command_on_libvirt_host test -d "$dir"; then
      run_command_on_libvirt_host mkdir -p "$dir"
    elif ! run_command_on_libvirt_host test -w "$dir"; then
      error_line "Directory \"$dir\" is not accessible!"
    fi
  done

  if ! check_curl_version "${min_curl_version[@]}"; then
    error_line "Requires curl version $(
      IFS="."
      echo "${min_curl_version[*]}"
    ) or higher."
  fi

  return $global_status
}

#########################
# VM and image handling #
#########################

# This function check whether a directory is empty or not. A non-existing
# directory is considered empty. If the path exists, but isn't a directory, it
# is considered not empty.
function directory_is_empty {
  local directory="$1"
  if run_command_on_libvirt_host test -e "$directory"; then
    if run_command_on_libvirt_host test -d "$directory"; then
      local -a files
      readarray -t files < <(run_command_on_libvirt_host compgen -G "$directory/*")
      [ "${#files[@]}" = '0' ]
    else
      # the path exists, but is neither a directory nor a symlink to a directory.
      false
    fi
  fi
}

# Start the given libvirt network if it isn't active yet.
function ensure_network_up {
  local network_name="$1"

  local -a lines
  run_and_log_debug virsh net-info --network "$network_name"
  LANG=C virsh net-info --network "$network_name" | readarray -t lines

  local label="Active:"
  local prefix_length="${#label}"

  local line network_status
  for line in "${lines[@]}"; do
    if [ "${line:0:$prefix_length}" = "$label" ]; then
      network_status="${line:$prefix_length}"
      if [[ "$network_status" != *"yes"* ]]; then
        info_line "Starting network \"$network_name\""
        run_and_log_debug virsh net-start --network "$network_name"
      fi
      return
    fi
  done
}

# This function will setup the virtual machine. As base image an official cloud
# image from the distributor will be used. Such image will be initially
# downloaded and cached in the image directory. Then the virtual machine will be
# installed and customized using cloud-init. This function terminates, when the
# virtual machine is fully installed and powered down.
# This function will set the variables 'machine_directory' and 'image_path'.
function setup_machine {
  machine_directory="$image_directory/$machine"
  image_path="$machine_directory/root.$image_format"

  if ! directory_is_empty "$machine_directory"; then
    info_line "Image directory \"$machine_directory\" already exists."
    if [ -n "$remove_existing" ]; then
      teardown_machine
    else
      error_line "Machine \"$machine\" seems to exist already. Refusing to continue!"
      info_line "Use parameter --remove to replace existing machine!"
      return 1
    fi
  fi

  # before creating anything, make sure the network is defined and up
  ensure_network_up "$network"

  run_command_on_libvirt_host mkdir -p "$machine_directory"

  local template_image_path="$image"
  if [ -z "$template_image_path" ]; then
    template_image_path="$template_directory/$template_image"
    if ! run_command_on_libvirt_host test -f "$template_image_path"; then
      info_line "Downloading template image \"$template_image\""
      run_and_log_command_on_libvirt_host curl \
        --no-progress-meter \
        --location \
        --remote-name \
        --output-dir "$template_directory" \
        "$template_url"
    fi
  fi

  info_line "Preparing disk image \"$image_path\""

  run_and_log_command_on_libvirt_host qemu-img convert -f qcow2 -O "$image_format" \
    "$template_image_path" "$image_path"
  run_and_log_command_on_libvirt_host qemu-img resize "$image_path" "$disk_size"

  local cloud_init_userdata_prefix="$machine_directory/userdata"

  local ssh_public_keys=()
  if command -v ssh-add >/dev/null; then
    ssh-add -L | readarray -t ssh_public_keys
  fi
  for file in ~/.ssh/**/*.pub; do
    ssh_public_keys+=("$(<"$file")")
  done

  debug_line "[CLOUD-INIT] using the following userdata:"
  (
    echo "#cloud-config"
    echo "hostname: $machine"
    echo "users:"
    echo "  - name: $user_name"
    echo "    sudo: ALL=(ALL) NOPASSWD:ALL"
    echo "    shell: /bin/bash"
    echo "    ssh_authorized_keys:"
    for ssh_public_key in "${ssh_public_keys[@]}"; do
      printf "      - %s\n" "${ssh_public_key}"
    done
    if [ "$distribution" = "debian" ] || [ "$distribution" = "ubuntu" ]; then
      echo "packages:"
      echo "  - openssh-server"
    fi
    if [ "$distribution" = "ubuntu" ]; then
      echo "  - cloud-initramfs-growroot"
    fi
    if [ "$distribution" = "ubuntu" ]; then
      echo "write_files:"
      echo "  - path : /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg"
      echo "    owner: root:root"
      echo "    permissions: '0644'"
      echo "    content: |"
      echo "      network:"
      echo "        config: disabled"
      echo "  - path : /etc/netplan/51-all-ifs.yaml"
      echo "    owner: root:root"
      echo "    permissions: '0644'"
      echo "    content: |"
      echo "      network:"
      echo "        version: 2"
      echo "        ethernets:"
      echo "          eth0:"
      echo "            dhcp-identifier: mac"
      echo "            dhcp4: yes"
    fi
    if [ "$distribution" = "fedora" ]; then
      echo "runcmd:"
      echo "  - hostnamectl --static set-hostname $machine"
    fi
    if [ "$distribution" = "ubuntu" ]; then
      echo "runcmd:"
      echo "  - ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf"
      echo "  - sed -E 's/GRUB_CMDLINE_LINUX=\\\"\\\"/GRUB_CMDLINE_LINUX=\\\"net.ifnames=0 biosdevname=0\\\"/' -i /etc/default/grub"
      echo "  - grub-mkconfig -o /boot/grub/grub.cfg"
    fi
    echo "power_state:"
    echo "  mode: poweroff"
  ) | run_and_log_command_on_libvirt_host tee "$cloud_init_userdata_prefix.cfg"

  run_command_on_libvirt_host cloud-localds -v "$cloud_init_userdata_prefix".{iso,cfg} 2>&1 | debug_log

  run_command_on_libvirt_host rm "$cloud_init_userdata_prefix.cfg"

  info_line "Setting up machine \"$machine\""

  run_and_log_debug virt-install \
    --name "$machine" \
    --vcpus "$cpus" \
    --memory "$mem_size" \
    --disk "$image_path" \
    --import \
    --cdrom "$cloud_init_userdata_prefix.iso" \
    --os-variant "$os_variant" \
    --network network="$network" \
    --metadata "$meta_data" \
    --noautoconsole

  wait_for_shutdown "$setup_timeout"

  run_and_log_command_on_libvirt_host rm -f "$cloud_init_userdata_prefix.iso"
}

function wait_for_shutdown {
  local timeout="$1"
  local count=0
  local state
  if state=$(LANG=C virsh domstate "$machine"); then
    debug_line "Current state: $state"
    while [ "$state" != "shut off" ]; do
      sleep 1
      state=$(LANG=C virsh domstate "$machine")
      ((count += 1))
      if ((count > timeout)); then
        debug_line "State: $state"
        error_line "Waiting for shutdown timed out"
        return 1
      fi
    done
  else
    debug_line "Machine \"$machine\" does not exist."
    return 1
  fi
}

# This function ensure that a VM is down using forceful shutdown
function shutdown_machine {
  local state
  if state=$(LANG=C virsh domstate "$machine" 2>/dev/null); then
    debug_line "Current state: $state"
    if [ "$state" = "running" ]; then
      debug_line "Shutting down \"$machine\""
      run_and_log_debug virsh destroy "$machine"
    fi
  fi
}

# This function will remove the VM, which is denoted in variable
# 'machine'. Snapshots and the disk image will be deleted.
function teardown_machine {
  if ! directory_is_empty "$machine_directory"; then
    info_line "Tearing down machine \"$machine\""

    if virsh domstate "$machine" >/dev/null 2>&1; then
      declare -a snapshots
      # In order to be able to catch the exit value of the command, we have to
      # use expansion here. Since snapshot names can't contain spaces, we're ok
      # without manipulation of IFS. But we have to disable globbing.
      set -f
      # shellcheck disable=SC2207
      if snapshots=($(virsh -q snapshot-list --domain "$machine" --name)); then
        set +f
        debug_line "Machine $machine contains snapshots"
        for snapshot in "${snapshots[@]}"; do
          delete_snapshot "$snapshot"
        done
      fi
      set +f
      debug_line "Deleting machine \"$machine\""
      shutdown_machine "$machine"
      run_and_log_debug virsh undefine --remove-all-storage "$machine"
    fi
    run_command_on_libvirt_host rm -rf "$image_path"
  fi

  # clean out storage pool
  local storage_pool
  for storage_pool in $(virsh pool-list --name --all); do
    if [ "$storage_pool" = "$machine" ]; then
      if ! virsh pool-info --pool "$storage_pool" | grep -P "^State:\s+inactive$"; then
        run_and_log_debug virsh pool-destroy --pool "$storage_pool"
      fi
      run_and_log_debug virsh pool-undefine --pool "$storage_pool"
    fi
  done

  run_command_on_libvirt_host rm -rf "$machine_directory"
}

# Takes a snapshot name and delete it from the VM machine.
function delete_snapshot {
  local snapshot=$1
  debug_line "Deleting snapshot $snapshot"
  run_and_log_debug virsh snapshot-delete --domain "$machine" --snapshotname "$snapshot"
}

##########################
# Command Line Interface #
##########################

function usage_print_parameter {
  local arg="$1"
  local description="$2"
  printf " %-25s %s\n" "$arg" "$description"
}

function usage_print_option_switch {
  local short="$1"
  local long="$2"
  local description="$3"
  local arg
  printf -v arg -- "-%s, --%s" "$short" "$long"
  usage_print_parameter "$arg" "$description"
}

function usage_print_optional_parameter {
  local short=$1
  local long=$2
  local variable=$3
  local description=$4
  local arg
  printf -v arg -- "-%s, --%s %s" "$short" "$long" "$variable"
  usage_print_parameter "$arg" "$description"
}

function usage_print_parameter_with_default {
  local short=$1
  local long=$2
  local variable=$3
  local help=$4
  local default_value=$5
  local arg description
  printf -v arg -- "-%s, --%s %s" "$short" "$long" "$variable"
  printf -v description -- "%s (default \"%s\")\n" "$help" "$default_value"
  usage_print_parameter "$arg" "$description"
}

function usage {
  local exit_status="$1"
  printf "\nUsage: %s [OPTIONS] [VM_NAME]\n" "$this"
  printf "\nDescription:\n This script can be used for quickly setting up a virtual machine
on libvirt.\n\n"
  print_supported_images | fmt -w 80
  printf "\nParameters:\n"
  usage_print_parameter "VM_NAME" "Specify the VM name (domain in libvirt terminology)"
  printf "\nOptions:\n"
  usage_print_parameter_with_default "p" "cpus" "CPUS" "number of cpu cores for the virtual machine" "$default_cpus"
  usage_print_parameter_with_default "m" "mem-size" "MEMORY" "amount of memory (in MiB) for the virtual machine" "$default_mem_size"
  usage_print_parameter_with_default "d" "disk-size" "DISK" "disk size for the virtual machine" "$default_disk_size"
  usage_print_parameter_with_default "o" "os" "DISTRIBUTION" "operating system to use as basis for the virtual machine" "$default_os_name"
  usage_print_parameter_with_default "f" "format" "FORMAT" "image format to use for the disk image" "$default_image_format"
  usage_print_optional_parameter "c" "connect" "URL" "connect to this libvirt URL (defaults to LIBVIRT_DEFAULT_URI, then \"$default_connection_url\")"
  usage_print_parameter_with_default "l" "pool" "POOL" "libvirt pool to use for image files" "$default_pool"
  usage_print_parameter_with_default "n" "network" "NETWORK" "libvirt network to use the virtual machine" "$default_network"
  usage_print_optional_parameter "u" "user" "USER_NAME" "setup administrative user account for this user on the virtual machine (defaults to current user)"
  usage_print_parameter_with_default "e" "metadata" "META_DATA" "meta data for the virtual machine (cf. virt-install --metadata)" ""
  usage_print_optional_parameter "i" "image" "IMAGE" "use the given image instead of an officially supported one"
  usage_print_option_switch "r" "remove" "in case remove already existing virtual machine with same name"
  usage_print_parameter_with_default "t" "setup-timeout" "TIMEOUT_IN_SECONDS" "timeout (in seconds) for awaiting setup of machine" "$default_setup_timeout"
  usage_print_option_switch "s" "start" "start virtual machine right after install"
  usage_print_option_switch "v" "verbose" "turn debug messages on"
  usage_print_option_switch "h" "help" "display this message"
  exit "$exit_status"
}

function process_args {
  local count=0
  cpus=$default_cpus
  mem_size=$default_mem_size
  disk_size=$default_disk_size
  os_name=''
  image_format=$default_image_format
  connection_url="$fallback_connection_url"
  pool="$default_pool"
  network="$default_network"
  user_name="$fallback_user_name"
  meta_data=''
  setup_timeout="$default_setup_timeout"
  remove_existing=''
  debug_output=''
  machine=''
  image=''
  start_after_install=''
  while (("$#")); do
    case "$1" in
      "-h" | "--help")
        usage 0
        ;;
      "-p" | "--cpus")
        cpus=$2
        shift
        ;;
      "-m" | "--mem-size")
        mem_size=$2
        shift
        ;;
      "-d" | "--disk-size")
        disk_size=$2
        shift
        ;;
      "-o" | "--os")
        os_name=$2
        shift
        ;;
      "-f" | "--format")
        image_format=$2
        shift
        ;;
      "-c" | "--connect")
        connection_url=$2
        shift
        ;;
      "-l" | "--pool")
        pool=$2
        shift
        ;;
      "-n" | "--network")
        network=$2
        shift
        ;;
      "-u" | "--user")
        user_name=$2
        shift
        ;;
      "-e" | "--metadata")
        meta_data=$2
        shift
        ;;
      "-r" | "--remove")
        remove_existing=yes
        ;;
      "-v" | "--verbose")
        debug_output=yes
        ;;
      "-i" | "--image")
        image=$2
        shift
        ;;
      "-s" | "--start")
        start_after_install=yes
        ;;
      -*)
        usage 1
        ;;
      *)
        case $count in
          0)
            machine=$1
            ;;
          *)
            usage 1
            ;;
        esac
        ((count = count + 1))
        ;;
    esac
    shift
  done
}

function parse_connection {
  remote_host=""
  remote_user=""
  remote_port=""
  remote_ssh_parameters=()

  # remote URI have the form
  # driver[+transport]://[username@][hostname][:port]/[path][?extraparameters]
  local valid_connection_pattern='^qemu(\+ssh)?://([^/]+@)?([^/|:]*)(:[^/]+)?/[^\?]*(\?.*)?$'

  if [[ "$connection_url" =~ $valid_connection_pattern ]] \
    && [ -z "${BASH_REMATCH[1]:-}" ] || [ -n "${BASH_REMATCH[3]:-}" ]
  then
    remote_user=${BASH_REMATCH[2]/@}
    remote_host=${BASH_REMATCH[3]}
    remote_port=${BASH_REMATCH[4]#:}
    [ -n "$remote_user" ] && remote_ssh_parameters+=('-l' "$remote_user")
    [ -n "$remote_port" ] && remote_ssh_parameters+=('-p' "$remote_port")
  else
    error_line "Libvirt URI »$connection_url« is not supported."
    error_line "Only qemu hypervisor, local or via SSH transport, is supported."
    return "$global_status"
  fi

  export LIBVIRT_DEFAULT_URI=$connection_url
}

function main {
  # clear global status variable
  global_status=0

  # check if running in a terminal
  if [ -t 0 ]; then
    error_level="\E[91m ERROR\E[0m"
    info_level="\E[96m INFO \E[0m"
    debug_level="\E[93m DEBUG\E[0m"
  fi

  process_args "$@"

  if [ -z "$machine" ]; then
    error_line "Machine name for the virtual machine is required."
    usage 1
  fi

  parse_connection

  # register termination function
  trap finally EXIT

  # check the environment
  check_environment

  if [ -z "$image" ]; then
    [ -n "$os_name" ] || os_name=$default_os_name

    os_check_supported "$os_name"

    # set up the machine
    distribution_set_details "$os_name"
  else
    os_variant='none'
    distribution=''
  fi

  summary | info_log
  setup_machine
  if [[ "$start_after_install" ]]; then
    run_and_log_debug virsh -c "$connection_url" start "$machine"
    info_line "Machine $machine is up and running"
  else
    info_line "Machine $machine is now ready. You can start it with"
    info_line "    virsh -c $connection_url start $machine"
  fi
}

function summary {
  echo "Setting up machine \"$machine\""
  printf "      CPUs: %10d    Memory: %8s\n" "$cpus" "$mem_size"
  printf " Disk Size: %10s    Format: %8s\n" "$disk_size" "$image_format"
  printf "        OS: %10s\n" "$os_name"
}

function finally {
  debug_line "Terminating with status $global_status"
  return $global_status
}

main "$@"
