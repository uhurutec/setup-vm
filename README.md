# setup-vm

The script `setup-vm.sh` can be used for quickly setting up a virtual machine
on *libvirt*.

Setting up a virtual machine manually for testing purpose can be tedious. There
are tools for setting up virtual machines in an automized way, e.g. vagrant, but
they often have many dependencies or require to write a configuration file in a
certain language.

This script uses cloud-init for customizations and therefore requires base
images from upstream, which support cloud-init. Those base images will be
downloaded and cached as required on the fly.


## Usage

The script takes one obligatory parameter: the name of the virtual machine
(*domain* in libvirt terminology).

```
$ ./setup-vm.sh test-machine
[ INFO] Setting up machine "test-machine"
[ INFO]       CPUs:          2    Memory:     4096
[ INFO]  Disk Size:        20G    Format:    qcow2
[ INFO]         OS:  debian-11
[ INFO] Preparing disk image "/var/lib/libvirt/images/test-machine/root.qcow2"
[ INFO] Setting up machine "test-machine"
[ INFO] Machine test-machine is now ready. You can start it with
[ INFO]     virsh -c qemu:///system start test-machine
```

For additional usage information see the help message of the script.

```
$ ./setup-vm.sh --help
```

## Accessing the Virtual Machine

You can access the started virtual machine via SSH using the same username,
which was used to execute `setup-vm.sh` or was provided in combination with
the `-u` parameter.

## Preconditions

### Dependencies

- *curl* for retrieving the base image
- `comm` from *coreutils*
- `fmt` from *coreutils*
- `getent` from glibc
- `id`, usually provided either by *coreutils* or by Busybox
- *qemu-img* for resizing disk images
- `cloud-localds` from *cloud-image-utils* for generating the configuration
  image for *cloud-init*
- `genisoimage` from *cdrtools* (a dependency of *cloud-image-utils*)
- *sed* (the Stream EDitor)
- `sort` from *coreutils*
- *virt-install* for setting up the virtual machine
- *virsh* for controlling *libvirt*
- `xmllint` from *libxml* for parsing libvirt XML data

On debian or its derivatives the required packages can be installed with:

```
# apt-get install cloud-image-utils curl libvirt-clients libvirt-daemon-system libxml2-utils openssh-client qemu-utils virtinst
```

### Remote and Hypervisor Support

The script supports Qemu/KVM hypervisor on the local machine and remote machine
via SSH transport by setting the connection URI (`-c` parameter). Other
hypervisors or transports are not supported. Refer to the [documentation][1] of
libvirt connection URIs for details.

### Permissions

The user `libvirt-qemu` must be able to access `/var/lib/libvirt/images`, which
has been reported not to hold by default on Ubuntu 22.04. This would manifest in
messages like

```
[ERROR] Directory "/var/lib/libvirt/images" is not accessible!
```

One solution is to use the existing `kvm` group for granting access:

```
# chown :kvm /var/lib/libvirt/images
# chmod 2775 /var/lib/libvirt/images
```

## License

This project is licensed under the *GNU Affero General Public License 3* - see
the [LICENSE](LICENSE) file for details

Copyright (C) 2022-2024 UhuruTec AG

---

[1]: https://libvirt.org/uri.html
